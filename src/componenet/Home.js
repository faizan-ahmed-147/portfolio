import React from "react";
import "../componenetCss/Home.css";
import homebg from "../img/bg.jpg"


function Home() {

  return (
    <>
      {/* HOME CONTENT */}

      <div className="container">
        <div className="home">
          <div className="home__content">
            <div className="home__meta">
              <h1 className="home__text pz__10">WELCOME TO MY WORLD</h1>
              <h2 className="home__text pz__10">Hi, I’m Faizan Ahmed</h2>
              <h4 className="home__text sweet pz__10">MERN STACK Developer.</h4>
              <h6 className="home__text pz__10">Lorem ipsum dolor sit amet consectetur adipisicing elit. <br />Iste quibusdam corporis voluptates deserunt,<br />  aperiam error suscipit fugiat corrupti eaque,<br /> delectus expedita nobis quam repellendus incidunt mollitia iure et?<br /> Quod est minima excepturi voluptatum rerum!</h6>
            </div>
            
              <img src={homebg} alt="" srcset="" />
            
          </div>
        </div>
      </div>

    </>
  );
}

export default Home;
