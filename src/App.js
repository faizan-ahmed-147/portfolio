import About from "./componenet/About";
import "./App.css";
import Blog from "./componenet/Blog";
import Contact from "./componenet/Contact";
import Footer from "./componenet/Footer";
import Home from "./componenet/Home";
import Project from "./componenet/Project";
import Service from "./componenet/Service";
import { Routes, Route } from "react-router-dom";
import Navbar from "./componenet/Navbar";
function App() {
  return (
    <>
    {/* <div className="App"> */}
       <Navbar />
       <Routes>
     
        <Route path="/" element={<Home />} />
        <Route path="/About" element={<About />} />
        <Route path="/Service" element={<Service />} />
        <Route path="/Project" element={<Project />} />
        <Route path="/Blog" element={<Blog />} />
        <Route path="/Contact" element={<Contact />} />
        
      </Routes>
       <Footer />
    </>
  );
}

export default App;
