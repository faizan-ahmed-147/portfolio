const express = require('express')

const app = express()
const cookieParser = require('cookie-parser');
const connectToMongo = require ("./db/conn");
connectToMongo();
const dotenv = require("dotenv")
dotenv.config({ path: './config.env'})
const port = process.env.PORT

app.use(express.json());
app.use('/', require('./router/auth'))


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })
  